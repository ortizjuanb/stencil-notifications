import { Component, Prop, State, h } from "@stencil/core";
import { TimeAgoProps } from "./time-ago.type";

enum SECONDS {
  IN_MIN = 60,
  IN_HOUR = 3600,
  IN_DAY = 86400,
  IN_WEEK = 604800,
}

enum TIME_FRAME {
  SECONDS = "seconds",
  MINUTES = "minutes",
  HOURS = "hours",
  DAYS = "days",
}

const DEFAULT_FORMAT =
  "%S {second/seconds|minute/minutes|hour/hours|day/days} ago";

const extractTimeFrame = (str: string): Map<TIME_FRAME, string> => {
  const result = `${str}`.match(/\{(.*?)\}/i) as [
    unknown,
    TIME_FRAME | undefined
  ];
  const [seconds, minutes, hours, days] =
    result && result[1] ? result[1].split("|") : [];

  return new Map([
    [TIME_FRAME.SECONDS, seconds || TIME_FRAME.SECONDS],
    [TIME_FRAME.MINUTES, minutes || TIME_FRAME.MINUTES],
    [TIME_FRAME.HOURS, hours || TIME_FRAME.HOURS],
    [TIME_FRAME.DAYS, days || TIME_FRAME.DAYS],
  ]);
};

const getTimeUnits = (
  str: string,
  dateVal: number
): [number, string, number] => {
  const timeFrames = extractTimeFrame(str);
  const secsAgo = Math.round((Date.now() - dateVal) / 1000);
  switch (true) {
    case secsAgo > SECONDS.IN_WEEK:
      return [
        dateVal,
        timeFrames.get(TIME_FRAME.DAYS) as string,
        SECONDS.IN_DAY * 1000,
      ];
    case secsAgo > SECONDS.IN_DAY - 1:
      return [
        Math.round(secsAgo / SECONDS.IN_DAY),
        timeFrames.get(TIME_FRAME.DAYS) as string,
        SECONDS.IN_DAY * 1000,
      ];
    case secsAgo > SECONDS.IN_HOUR - 1:
      return [
        Math.round(secsAgo / SECONDS.IN_HOUR),
        timeFrames.get(TIME_FRAME.HOURS) as string,
        SECONDS.IN_HOUR * 1000,
      ];
    case secsAgo > SECONDS.IN_MIN - 1:
      return [
        Math.round(secsAgo / SECONDS.IN_MIN),
        timeFrames.get(TIME_FRAME.MINUTES) as string,
        SECONDS.IN_MIN * 1000,
      ];
    default:
      return [secsAgo, timeFrames.get(TIME_FRAME.SECONDS) as string, 10000];
  }
};

const formatText = (str: string, [time, unit]: [number, string, number]) => {
  const [singular, plural] = unit.split("/");
  const finalUnit = time !== 1 ? plural || singular : singular;
  if (time > SECONDS.IN_WEEK) return new Date(time).toLocaleDateString();
  return str
    .replace("%S", time.toString())
    .replace(/\{(.*?)\}/i, finalUnit as string);
};

@Component({
  tag: "time-ago",
  shadow: true,
})
export class TimeAgo implements TimeAgoProps {
  /**
   * timestamp milliseconds: Date.now()
   */
  @Prop() starts: number = Date.now();

  /**
   * suffix message to replate
   */
  @Prop() suffix?: string = "";

  /**
   * format: "%S {second/seconds|minute/minutes|hour/hours|day/days} ago %E"
   */
  @Prop() format: string = DEFAULT_FORMAT;

  /**
   * timer: private
   */
  private timer: number;
  private initial: number;

  @State() time: number = Date.now();

  connectedCallback() {
    this.initial = +this.starts || Date.now();
    const format = this.format || DEFAULT_FORMAT;
    const [_a, _b, every] = getTimeUnits(format, this.initial);
    this.timer = window.setInterval(() => {
      this.time = Date.now();
    }, every);
  }

  disconnectedCallback() {
    window.clearInterval(this.timer);
  }

  private getText(): string {
    const units = getTimeUnits(this.format, this.initial);
    return formatText(this.format || DEFAULT_FORMAT, units);
  }

  render() {
    return <span>{this.getText()}</span>;
  }
}
