import { StorybookTemplate } from "../../utils/storybook.utils";
import { TimeAgoProps } from "./time-ago.type";

export default {
  title: "Components/TimeAgo",
};

const Template = StorybookTemplate<TimeAgoProps>("time-ago");

export const Default = Template.bind({});
Default.args = {};

export const Past = Template.bind({});
Past.args = {
  starts: Date.now() - 14000,
  format: "%S {second/seconds|minute/minutes|hour/hours|day/days} ago",
} as TimeAgoProps;

export const Spanish = Template.bind({});
Spanish.args = {
  starts: Date.now(),
  format:
    "Ocurrió hace %S {segundo/segundos|minuto/minutos|hora/horas|dia/dias}",
} as TimeAgoProps;
