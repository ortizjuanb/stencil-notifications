# time-ago



<!-- Auto Generated Below -->


## Properties

| Property | Attribute | Description                                                                | Type     | Default          |
| -------- | --------- | -------------------------------------------------------------------------- | -------- | ---------------- |
| `format` | `format`  | format: "%S {second/seconds\|minute/minutes\|hour/hours\|day/days} ago %E" | `string` | `DEFAULT_FORMAT` |
| `starts` | `starts`  | timestamp milliseconds: Date.now()                                         | `number` | `Date.now()`     |
| `suffix` | `suffix`  | suffix message to replate                                                  | `string` | `""`             |


## Dependencies

### Used by

 - [notification-panel-item](../notification-panel-item)

### Graph
```mermaid
graph TD;
  notification-panel-item --> time-ago
  style time-ago fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
