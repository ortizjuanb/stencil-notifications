export interface TimeAgoProps {
  starts: number;
  suffix?: string;
  format: string;
}
