import { Notification } from "../../types/notification.type";

export interface NotificationPanelItemProps {
  /**
   * notification item
   */
  setNotification: (n: Notification) => void;

  /**
   * variant
   */
  variant: "unread" | "read";
}
