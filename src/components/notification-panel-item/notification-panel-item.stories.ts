import { StorybookTemplate } from "../../utils/storybook.utils";
import { NotificationPanelItem } from "./notification-panel-item";
import { NotificationPanelItemProps } from "./notification-panel-item.type";

export default {
  // this creates a ‘Components’ folder and a ‘MyComponent’ subfolder
  title: "Components/NotificationPanelItem",
  // this is an example of how to add actions to the story
  parameters: {
    actions: {
      argTypesRegex: "^on.*",
      handles: ["click"], // you can add custom events to this array to trigger actions
    },
  },
};

const Template = StorybookTemplate<NotificationPanelItemProps>(
  "notification-panel-item"
);

export const Default = Template.bind({});
Default.args = {};

export const Read = Template.bind({});
Read.args = {
  variant: "read",
  storybookInitComponent: (c: NotificationPanelItem) => {
    c.setNotification({
      title: "Notification read",
      description: "Here goes some larger description about the notification",
      createdAt: Date.now(),
      expiresAt: Date.now(),
      onClick: console.log,
    });
  },
};


export const Unread = Template.bind({});
Unread.args = {
  variant: "unread",
  storybookInitComponent: (c: NotificationPanelItem) => {
    c.setNotification({
      title: "Notification unread",
      description: "Some other description here",
      createdAt: Date.now(),
      expiresAt: Date.now(),
      onClick: console.log,
    });
  },
};
