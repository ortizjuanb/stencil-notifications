import { Component, Host, Method, Prop, State, h } from "@stencil/core";
import { Notification } from "../../types/notification.type";
import { TimeAgoProps } from "../time-ago/time-ago.type";
import { NotificationPanelItemProps } from "./notification-panel-item.type";

@Component({
  tag: "notification-panel-item",
  styleUrl: "notification-panel-item.css",
  shadow: true,
})
export class NotificationPanelItem
  implements TimeAgoProps, NotificationPanelItemProps
{
  private getTitle(): string {
    return this.notification?.title || "Notification";
  }

  private getBody(): string {
    return this.notification?.description || "";
  }

  private getStart(): number {
    return this.notification?.createdAt || Date.now();
  }

  @Prop() starts: number;
  @Prop() suffix?: string;
  @Prop() format: string;

  @Prop() variant: "unread" | "read" = "unread";
  @Prop() notif: Notification;

  @State() notification: Notification | null = null;

  @Method()
  public async setNotification(n: Notification) {
    this.notification = n;
    return;
  }

  connectedCallback() {
    if (this.notif) this.notification = this.notif;
  }

  render() {
    return (
      <Host>
        <li part="variant" onClick={this.notification?.onClick}>
          <div part="heading">{this.getTitle()}</div>
          <div part="body">{this.getBody()}</div>
          <div part="footer">
            <time-ago
              format={this.format}
              suffix={this.suffix}
              starts={this.getStart()}
            ></time-ago>
          </div>
        </li>
      </Host>
    );
  }
}
