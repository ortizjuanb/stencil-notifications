# notification-panel-item



<!-- Auto Generated Below -->


## Properties

| Property  | Attribute | Description | Type                                                                                                                   | Default     |
| --------- | --------- | ----------- | ---------------------------------------------------------------------------------------------------------------------- | ----------- |
| `format`  | `format`  |             | `string`                                                                                                               | `undefined` |
| `notif`   | --        |             | `{ title: string; description: string; onClick: () => void; createdAt: number; expiresAt: number; isSeen?: boolean; }` | `undefined` |
| `starts`  | `starts`  |             | `number`                                                                                                               | `undefined` |
| `suffix`  | `suffix`  |             | `string`                                                                                                               | `undefined` |
| `variant` | `variant` | variant     | `"read" \| "unread"`                                                                                                   | `"unread"`  |


## Methods

### `setNotification(n: Notification) => Promise<void>`

notification item

#### Returns

Type: `Promise<void>`




## Shadow Parts

| Part        | Description |
| ----------- | ----------- |
| `"body"`    |             |
| `"footer"`  |             |
| `"heading"` |             |
| `"variant"` |             |


## Dependencies

### Used by

 - [notification-panel](../notification-panel)

### Depends on

- [time-ago](../time-ago)

### Graph
```mermaid
graph TD;
  notification-panel-item --> time-ago
  notification-panel --> notification-panel-item
  style notification-panel-item fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
