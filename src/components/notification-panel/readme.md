# notification-panel-item



<!-- Auto Generated Below -->


## Methods

### `setNotifications(n: Notification[]) => Promise<void>`



#### Returns

Type: `Promise<void>`




## Shadow Parts

| Part        | Description |
| ----------- | ----------- |
| `"handler"` |             |
| `"list"`    |             |
| `"nav"`     |             |


## Dependencies

### Depends on

- [notification-panel-item](../notification-panel-item)

### Graph
```mermaid
graph TD;
  notification-panel --> notification-panel-item
  notification-panel-item --> time-ago
  style notification-panel fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
