import { StorybookTemplate } from "../../utils/storybook.utils";
import { NotificationPanel } from "./notification-panel";
import { NotificationPanelProps } from "./notification-panel.type";

export default {
  // this creates a ‘Components’ folder and a ‘MyComponent’ subfolder
  title: "Components/NotificationPanel",
  // this is an example of how to add actions to the story
  parameters: {
    actions: {
      argTypesRegex: "^on.*",
      handles: ["click"], // you can add custom events to this array to trigger actions
    },
  },
};

const Template =
  StorybookTemplate<NotificationPanelProps>("notification-panel");

export const Default = Template.bind({});
Default.args = {};

export const WithNotifications = Template.bind({});
WithNotifications.args = {
  storybookInitComponent: (c: NotificationPanel) => {
    c.setNotifications([
      {
        title: "Notification A",
        description: "Some description",
        createdAt: Date.now() - 12000,
        expiresAt: Date.now(),
        isSeen: true,
        onClick: console.log,
      },
      {
        title: "Notification B",
        description: "Some other description",
        createdAt: Date.now() - 1200,
        expiresAt: Date.now(),
        onClick: console.log,
      },
      {
        title: "Notification C",
        description: "A third description",
        createdAt: Date.now(),
        expiresAt: Date.now(),
        onClick: console.log,
      },
    ]);
  },
};
