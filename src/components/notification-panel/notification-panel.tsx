import { Component, Host, Method, State, h } from "@stencil/core";
import { Notification } from "../../types/notification.type";
import { NotificationPanelProps } from "./notification-panel.type";

@Component({
  tag: "notification-panel",
  styleUrl: "notification-panel.css",
  shadow: true,
})
export class NotificationPanel implements NotificationPanelProps {
  ref: HTMLElement;

  private toggleExpanded() {
    this.ref.classList.toggle("expanded");
  }

  @State() notifications: Notification[] = [];

  @Method()
  public async setNotifications(n: Notification[]) {
    this.notifications = n;
    return;
  }

  render() {
    return (
      <Host>
        <nav ref={(el) => (this.ref = el)} part="nav">
          <ul part="list">
            {this.notifications.map((notification) => (
              <notification-panel-item
                variant={notification.isSeen ? "read" : "unread"}
                notif={notification}
                exportparts="variant"
              ></notification-panel-item>
            ))}
          </ul>
          <div
            class="handler"
            part="handler"
            onClick={this.toggleExpanded.bind(this)}
          ></div>
        </nav>
      </Host>
    );
  }
}
