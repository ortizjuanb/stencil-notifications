export type StorybookTemplateReturn<T> = T & {
  storybookInitComponent: (c) => void;
};

export const StorybookTemplate =
  <T>(component: string) =>
  (args: T) => {
    const params = Object.entries(args)
      .filter(([_key, val]) => typeof val !== "function")
      .reduce((acc, [key, val]) => `${acc} ${key}="${val || ""}"`, "");

    window.addEventListener("storybookLoaded", (ev) => {
      const comp = ev["detail"];
      if (comp.nodeName.toLowerCase() !== component) return;
      const initFnCall = args["storybookInitComponent"];
      if (comp && initFnCall) initFnCall(comp);
    });

    return `
      <${component} ${params}></${component}>
      <script>
        (async () => {
          await customElements.whenDefined("${component}");
          const component = document.querySelector("${component}");

          const storybookLoadedEvent = new CustomEvent("storybookLoaded", {
            bubbles: false,
            detail: component,
          });

          window.dispatchEvent(storybookLoadedEvent)
        })();
      </script>
  `;
  };
