export type Notification = {
  title: string;
  description: string;
  onClick: () => void;
  createdAt: number;
  expiresAt: number;
  isSeen?: boolean;
};
